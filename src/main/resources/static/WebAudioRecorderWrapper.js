class WebAudioRecorderWrapper {
    __constructor(){
        this.gumStream=null;
        this.recorder=null;
    }
    startRecording(finishCallback) {
        URL = window.URL || window.webkitURL;
        var input; 							//MediaStreamAudioSourceNode  we'll be recording
        var encodingType; 					//holds selected encoding for resulting audio (file)
        var encodeAfterRecord = true;       // when to encode

        // shim for AudioContext when it's not avb. 
        var AudioContext = window.AudioContext || window.webkitAudioContext;
        var audioContext; //new audio context to help us record

        var constraints = { audio: true, video: false }

        /*
            We're using the standard promise based getUserMedia() 
            https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
        */
        let self=this;
        navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
            console.log("getUserMedia() success, stream created, initializing WebAudioRecorder...");
            /**
             * sampleRate must be set when initializing AudioContext
             * after that, it is readonly
             */
            audioContext = new AudioContext({
                sampleRate: 11025
            });
            self.gumStream = stream;
            input = audioContext.createMediaStreamSource(stream);
            encodingType = "wav";

            self.recorder = new WebAudioRecorder(input, {
                workerDir: "static/web-audio-recorder-js-master/lib/", // must end with slash
                encoding: encodingType,
                numChannels: 1, //2 is the default, mp3 encoding supports only 2,
                onEncoderLoading: function (recorder, encoding) {
                    console.log("Loading " + encoding + " encoder...");
                },
                onEncoderLoaded: function (recorder, encoding) {
                    console.log(encoding + " encoder loaded");
                }
            });
            
            self.recorder.onComplete = function (recorder, blob) {
                console.log("Encoding complete");
                let url=self.createDownloadLink(blob, recorder.encoding);
                finishCallback(url, blob);
            }
            self.recorder.setOptions({
                timeLimit: 120,
                encodeAfterRecord: encodeAfterRecord,
                ogg: { quality: 0.5 },
                mp3: { bitRate: 160 }
            });

            console.log(self.recorder);
            //start the recording process
            self.recorder.startRecording();

            console.log("Recording started");

        }).catch(function (err) {
            //enable the record button if getUSerMedia() fails
            console.log(err);
        });
    }

    stopRecording() {
        console.log("stopRecording() called");
        //stop microphone access
        this.gumStream.getAudioTracks()[0].stop();
        this.recorder.finishRecording();
        console.log('Recording stopped');
    }

    createDownloadLink(blob, encoding) {
        var url = URL.createObjectURL(blob);
        return url;
    }
}